# Template matching

[[_TOC_]]

Per confrontare immagini è possibile fare operare mediante un confronto diretto oppure mediante template matching.

## Confronto diretto

Ogni immagine può essere considerata come un punto in uno spazio multidimensionale (ad es. per immagini grayscale si parla di vettori con n=WxH dimensioni).

Il confronto diretto prevede di calcolare la distanza fra tali vettori, ma possono presentarsi dei problemi:
- differenze di traslazione, rotazione, scala e prospettiva
- deformazione e variabilità dei pattern
- differenza di illuminazione
- presenza di rumore
- utilizzo di tecniche di acquisizioni diverse

## Template matching

Il template matching prevede invece di costruire uno o più pattern modello (template) che possono essere "cercati" all'interno dell'immagine, misurandone il grado di "somiglianza" (matching) in tutte le possibili posizioni.

Due possibili approcci, ognuno con le rispettive tecniche: template matching rigido e template matching deformabile.

### Template matching rigido

Il template T è costituito da un oggetto rigido (tipicamente un'immagine raster di dimensione inferiore a quella dell'immagine a cui verrà sovrapposta).

Il confronto avviene direttamente fra i pixel, ma a seconda dell'applicazione, può essere più efficace eseguire l'operazione dopo aver estratto determinate feature (es. gli edge oppure l'orientazione del gradiente).

Il template viene sovrapposto all'immagine in tutte le possibili posizioni (rispetto agli assi x e y) ma, anche in questo caso, in base all'applicazione può essere necessario ruotarlo e/o scalarlo.

Un template T a cui viene applicata una trasformazione si indica con $`T_{i}`$.

Per ogni istanza di $`T_{i}`$, il grado di similarità viene calcolato massimizzando la correlazione con la porzione di imamgine coperta da $`T_{i}`$.

[Come nella convoluzione](https://gitlab.com/emrevoid/uni/fei/course/-/blob/master/01_OPERAZIONI_IMMAGINI.md#aspetti-implementativi) è necessario decidere come comportarsi sui pixel di bordo (ovvero dove $`T_{i}`$ non è completamente contenuto nell'immagine).

Il rigid template matching può essere operato mediante due tecniche: la correlazione e la trasformata di Hough.

#### Correlazione

Per le formule si consultino le slide.

Sia $`I_{xy}`$ la porzione di immagine con le stesse dimensioni di $`T_{i}`$ centrata nel pixel (x, y).

Una misura intuitiva di diversità è la "Sum of Squared Differences" (SSD) in cui, se le norme di $`I_{xy}`$ e $`T_{i}`$ fossero costanti, determinarne il suo minimo equivarrebbe a massimiizzare la "Cross Correlation" (CC).

Se $`I_{xy}`$ e $`T_{i}`$ non sono costanti, le misure di correlazione vanno normalizzate. Ciò può capitare in quanto
- diverse regioni della stessa immagine raramente sono costanti
- istanze dello stesso template possono essere diverse tra loro per numero di pixel e luminosità media

Si parla quindi di Normalized SSD (NSSD) e Normalized CC (NCC). NSSD e NCC non vengono impattate dal contrasto (ovvero da ampi range di livelli di grigio), a differenza della SSD semplice in cui pattern ad elevato contrasto vengono considerati più dissimili rispetto a pattern con scarso contrasto.

Infine esistono le Zero-mean NSSD (ZNSSD) e Zero-mean NCC (ZNCC) che non vengono impattate nemmeno da luminosità medie diverse.

##### Problematiche

- **Complessita computazionale**: È evidente che considerare, per ogni template, diverse decine di istanze trasformate per scala e rotazione che devono poi essere sovrapposte all'immagine in tutte le possibili posizioni porta ad un numero di combinazioni estremamente elevato. Ciò richiederà quindi tempi di esecuzione che non sono compatibili con applicazioni real-time.
- **Difficile gestione di pattern deformabili**: è richiesta invarianza per posizione, rotazione, sala, rapporto d'aspetto (lunghezza / altezza), gestione di colore e deformazioni locali. Di base la correlazione non risponde ai requisiti.

##### Approccio multi-risoluzione

Ha come obiettivo la riduzione della complessità computazionale.

Si basa sulla ricerca su una gerarchia crescente di risoluzioni (sia per l'immagine che per il template), a partire da quella più bassa e si procede con l'analisi ai livelli più alti solo se la correlazione supera una certa soglia.

#### Trasformata di Hough

Nella sua versione originale si tratta di un robusto metodo per l'individuazione di linee rette in un'immagine.

Le sue generalizzazioni possono portare a cercare figure di forma arbitraria (circonferenze, ellissi...).

### Template matching deformabile

Diverse tecniche:

- **free-form**: il template non è vincolato a forme precise. Le feature salienti dell'immagine producono una funzione di energia (potential field) che guida il processo verso le deformazioni maggiormente significative
- **parametric**: si ottengono deformazioni controllate a partire da template formati da archi, curve e regolati da un numero limitato di parametri
- **shape learning**: si "apprendono" in modo automatico le possibili variazioni della forma del template a partire da esempi rappresentativi delle possibili variazioni dell'oggetto che il template deve rappresentare 
