# Estrazione dei Bordi e Segmentazione

[[_TOC_]]

## Estrazione dei Bordi

**Bordo**: separazione oggetto / sfondo oppure oggetto / oggetto.

È una informazione più stabile rispetto ad altri parametri come colore e tessitura in merito ad illuminazione, rumore...

Serve per interpretare la forma geometrica di un oggetto, infatti l'operazione di **edge detection** è spesso il primo passo per l'individuazione dell'oggetto.

**Edge detection**: basata su filtri derivativi. Il valore in ogni punto, infatti, è una **stima numerica** del gradiente nel pixel corrispondente.

**Derivata di un segnale**: ne indica la sua variabilità (locale). Nel caso di **immagini** i segnali sono bidimensionali, per cui si eseguono derivate parziali.

Il **gradiente** è un vettore le cui componenti sono le derivate parziali nelle diverse direzioni.

Il gradiente è quindi utile per estrarre i bordi poiché questi ultimi corrispondono a variazioni di luminosità, e quindi effettuare l'operazione di derivata permette proprio di raggiungere l'obiettivo.

Derivata di un'immagine in un punto:

```math
\nabla I[x, y] = \left[ \frac{\delta I[x, y]}{\delta x}, \frac{\delta I[x, y]}{\delta y} \right]
\\ = \left[ I[x + 1, y] - I[x, y], I[x, y + 1] - I[x, y] \right]
```

dove nel secondo passaggio abbiamo applicato il limite del rapporto incrementale.

Il gradiente è **orientato** in direzione in cui la variazione è massima.

La direzione dell'**edge** è ortogonale all'orientazione del gradiente.

```math
\theta(x, y) = atan_{q}(\nabla y, \nabla x)
```

dove $`atan_{q}`$ indica l'arcotangente in quadranti.

```math
g = \sqrt{\nabla x^{2} + \nabla y^{2}}
```

### Come estrarre i bordi

Data l'immagine originale, si procede con i seguenti passi:
1. calcolo del modulo del gradiente con uno degli operatori descritti in seguito
2. binarizzazione con soglia determinata sperimentalmente
3. gli edge trovati si sovrappongono all'immagine originale

### Operatori di Roberts

- filtri convoluzionali 2x2
- origine (0, 0) in alto a sx
- assi ruotati di 45°
- usati nei vecchi calcolatori
  - pro: calcoli efficienti
  - contro: sensibili al rumore

Siano

I

| A | B |
|---|---|
| C | D |

$`F_{x}`$

| +1 | 0 |
|----|---|
| 0 | -1 |

e $`F_{y}`$

| 0 | +1 |
|----|---|
| -1 | 0 |

e quindi $`\nabla x = D - A`$ e $`\nabla y = C - B`$

```math
|| \nabla || = \sqrt{\nabla x^2 + \nabla y^2} \simeq |\nabla x| + |\nabla y|
\\ \theta = atan_{q}(\nabla x, \nabla y) + \frac{\pi}{4}
```

dove $`\frac{\pi}{4}`$ serve a sistemare la rotazione degli assi.

### Operatori di Prewitt

- due filtri convoluzionali 3x3
- simmetrici rispetto al punto di applicazione
- assi orientati in modo tradizionale
- meno sensibili a variazione di luce e rumore

Come funziona:
1. Calcola il gradiente lungo una direzione
2. Fa la media locale (smooth) lungo la direzione ortogonale

Siano

I

| A | B | C |
|---|---|---|
| D | E | F |
| G | H | I |

$`F_{x}`$

| 1 | 0 | -1 |
|---|---|---|
| 1 | 0 | -1 |
| 1 | 0 | -1|

e $`F_{y}`$


| 1 | 1 | 1 |
|---|---|---|
| 0 | 0 | 0 |
| -1 | -1 | -1|

e quindi $`\nabla x = \frac{1}{3} \cdot [(C + F + I) - (A + D + G)]`$ e $`\nabla x = \frac{1}{3} \cdot [(G + H + I) - (A + B + C)]`$

### Operatori di Sobel

- come Prewitt
- applica più peso al pixel centrale

$`F_{x}`$

| 1 | 0 | -1 |
|---|---|---|
| 2 | 0 | -2 |
| 1 | 0 | -1|

e $`F_{y}`$


| 1 | 2 | 1 |
|---|---|---|
| 0 | 0 | 0 |
| -1 | -2 | -1|

e va moltiplicato per 1/4

### Canny edge detector

Utilizzando solamente dei filtri per individuare ed estrarre i bordi si possono incontrare i seguenti problemi:
- frammenti di edge non connessi
- punti e segmenti "spuri" dati da rumore o piccole variazioni locali

Si procede dunque con un approccio più complesso, che prevede:
1. smoothing gaussiano dell'immagine al fine di rimuovere il rumore
2. calcolo del gradiente (con l'operatore di Prewitt)
3. soppressione dei non-massimi in direzione ortogonale all'edge
4. selezione degli edge significativi mediante isteresi
5. thinning (opz. vedi passo 3)

Parametri usati dal processo:
- $`\sigma`$: per l'ampiezza della gaussiana nella prima fase
- Dimensione del filtro nella prima fase
- T1 e T2: soglie per l'isteresi nell'ultima fase

#### 1: smoothing gaussiano

Si applica un filtro

```math
G_{2D} (x, y, \sigma) = \frac{1}{2 \pi \sigma^2} \cdot exp^{-\frac{x^2 + y^2}{2\sigma^2}}
```

Il filtro è separabile

```math
G_{1D} (t, \sigma) = \frac{1}{\sqrt{2 \pi \sigma}} \cdot exp^{-\frac{t^2}{2\sigma^2}}
\\ G_{2D} (x, y, \sigma) = G_{1D} (x, \sigma) \cdot G_{1D} (y, \sigma)
```

dove il termine $`\frac{1}{\sqrt{2 \pi \sigma}}`$ è trascurabile poiché dopo il calcolo bisognerà comunque normalizzare gli elementi rispetto alla somma dei pesi.

I valori possono essere approssimati in intero per maggiore efficienza

#### 2: Calcolo del gradiente

Avendo ridotto / eliminato il rumore con lo smoothing gaussiano, converrebbe usare gli operatori di Roberts (che erano efficienti in termini di calcolo ma sensibili, apppunto, al rumore).

Tuttavia si usano gli operatori di Prewitt poiché non necessitano di "sistemare" la rotazione degli assi.

#### 3: soppressione dei non-massimi

- si analizza l'intorno 3x3 di ogni pixel
- controlla i pixel lungo la direzione del gradiente
- se non sono massimi locali, li elimina

In particolare la stima del modulo del gradiente viene effettuata nei punti p1 e p2 mediante interpolazione lineare

```math
|| \nabla p_{1} || \simeq d \cdot || \nabla p_{B} || + (1 - d) \cdot || \nabla p_{A} ||
\\
|| \nabla p_{2} || \simeq d \cdot || \nabla p_{F} || + (1 - d) \cdot || \nabla p_{E} ||
```

con $`d = \frac{\nabla y[p]}{\nabla x[p]}`$

Dunque il pixel p viene conservato solo se $`|| \nabla p || \geq || \nabla p_{1} || \land || \nabla p || \geq || \nabla p_{2} ||`$

Questa tecnica non garantisce edge di spessore unitario, che possono comunque essere ottenuti ricorrendo al thinning al termine dell'intero algoritmo

#### 4: selezione finale degli edge

Obiettivi:
- selezionare gli edge significativi
- tralasciare edge "spuri"
- pur evitando frammentazione

Procedura di isteresi: siano T1 e T2 due soglie con T1 > T2 in [0, 1] ($`|| \nabla p ||`$, il modulo del gradiente, va normalizzato nello stesso intervallo per poter effettuare il confronto con le due soglie).

p viene mantenuto se
- $`|| \nabla p || > T_{1}`$
- $`T_{1} > || \nabla p || > T_{2}`$ && p adiacente a pixel validi

## Segmentazione

Operazione di separazione di uno o più oggetti dallo sfondo.

### mediante soglia globale

Si sceglie una soglia globale determinata come minimo locale tra due picchi dell'istogramma.

Lo smoothing dell'istogramma e la sua rappresentazione come radice quadrata possono facilitare l'operazione.

### mediante soglia locale

La soglia globale non è efficace, specie in presenza di oggetti e sfondi non uniformi o se la luminosità varia da una zona all'altra dell'immagine.

Per cui si procede nel determinare una soglia per ogni pixel considerando una porzione dell'immagine.

Approcci possibili:
- si divide l'immagine in regioni (meglio se parzialmente sovrapposte) e su ognuna di esse calcolare la soglia mediante analisi dell'istogramma
- si divide ricorsivamente l'immagine in regioni finché il loro istogramma presenta distintamente due picchi
- per ogni pixel si determina la soglia analizzando i pixel in un intorno (ad esempio un intorno quadrato di lato s)
- altre tecniche sulle slide...

Cose che possono aiutare:
- sottrazione dello sfondo (se con illuminazione costante e se catturato in assenza dell'oggetto)
- utilizzo del colore, analizzando la componente Hue per isolare la parte di interesse
