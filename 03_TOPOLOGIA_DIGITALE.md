# Topologia digitale

[[_TOC_]]

Disciplina che studia proprietà e caratteristiche topologiche delle immagini (es. componenti connesse, bordi di un oggetto...).

Lavora su immagini **binarie**.

Le immagini binarie sono così caratterizzate:
- hanno un foreground (tipicamente 255 o 0)
- hanno un background (tipicamente 0 o 255) e si indicano come $`\ne`$ foreground

Sono definiti i seguenti insiemi:
- F insieme di tutti i pixel di foreground $`F = \{ p | p = [x,y]^T, Img[y,x]=foreground\}`$
- F insieme di tutti i pixel di background $`F^{*} = \{ p | p = [x,y]^T, Img[y,x] \neq foreground \}`$

## Metriche (discrete) e distanze

**Metrica**: modo di calcolare una distanza.

**Distanza**: differenza tra due valori.

**Vicini** di un pixel **p**: sono quelli che hanno distanza unitaria da **p**.

Metriche più comuni:
- $`d_{4}`$: city-block: 4 vicini destra, sinistra, su, giù
- $`d_{8}`$: chessboard: 8 vicini, come d4 più le diagonali

**Percorso**: di lunghezza n da p a q è una sequenza di pixel tale che, in accordo con la metrica scelta, $`p_{i}`$ è un vicino di $`p_{i+1}`$ con $`0 \leq i < n`$

**Componente connessa**: è un sottoinsieme di F o di $`F^{\ast}`$ tale che, presi due qualsiasi dei suoi pixel, esiste tra questi un percorso appartenente a F o $`F^{\ast}`$. A seconda della metrica adottata si parla di 4-connessione o di 8-connessione.

L'immagine è memorizzata come un array monodimensionale e per scorrere i pixel, facendo un compromesso tra semplicità ed efficienza, si usa una classe cursore che eventualmente ignora i bordi e permetta di accedere ai bordi secondo la metrica definita.

## Trasformata distanza

La trasformata distanza di F rispetto a $`F^{\ast}`$ è una replica di F in cui i pixel sono etichettati con il valore della loro distanza da $`F^{\ast}`$ calcolata secondo una data metrica.

Nel caso della metrica $`d_{4}`$ la trasformata distanza si calcola effettuando due scansioni, una diretta (alto -> basso, sx -> dx) e una inversa (basso -> alto, dx -> sx) in cui si trasformano i pixel di F mentre i pixel di $`F^{\ast}`$ sono posti a zero (distanza nulla).

### Scansione diretta

$`\forall p \in F`$, il valore trasformato Img'[p] è calcolato come il minimo della distanza letta dai vicini Nord e Ovest (ovvero quelli scansionati precedentemente) + 1: $`Img'[p] = min\{Img'[p_{o}], Img'[p_{n}] \} + 1`$.

### Scansione inversa

Si scorre l'immagine di nuovo al contrario, aggiornando le distanze: $`Img'[p] = min\{Img'[p_{e}] + 1, Img'[p_{s}] + 1, Img'[p] \}`$

### Applicazioni

- misurazione geometrica di lunghezze, spessori...
- produce massimi locali di intensità in corrispondenza degli assi mediani dell'oggetto (scheletro dell'oggetto)
- template matching
- robotica (ricerca direzione di movimento, aggiramento ostacoli...)

## Estrazione del contorno

**Contorno**: insieme di pixel che hanno distanza unitaria da $`F^{\ast}`$ secondo la metrica adottata per $`F^{\ast}`$.

La scelta di una metrica $`d_{4}`$ per F implica una metrica $`d_{8}`$ per $`F^{\ast}`$ e viceversa.

L'operazione di estrazione del contorno si basa su una semplice tecnica che prevede di percorrere il bordo nella stessa direzione fino a incontrare il pixel di partenza.

### Algoritmo

Consideriamo metrica $`d_{4}`$ per F, $`d_{8}`$ per $`F^{\ast}`$ e verso di percorrenza antioraria del bordo).

Siano **p** il pixel corrente e **d** la direzione corrente di inseguimento (il cui valore dipende dall'indice del pixel da cui si è giunti a p).

L'algoritmo prevede che il prossimo pixel da visitare è uno dei 4 vicini a p, in particolare è il primo tra i seguenti che appartiene a F: $`\{ n_{(d+1) mod 4}, n_{(d+2) mod 4}, n_{(d+3) mod 4}, n_{(d+4) mod 4} \}`$.

In sostanza si vuole prendere il primo pixel di bordo nella direzione il più possibile "chiusa" in senso antiorario rispetto a d+180°.

### Struttura dati per il contorno

Possibili strutture dati per codificare il bordo:
- lista delle coordinate (x, y)
  - non vantaggiosa
- lista degli indici dei pixel (intesi come offset nell'array monodimensionale)
  - poco conveniente: dipende dalla larghezza dell'immagine
- chain code (lista degli indici delle direzioni)
  - permettono di generare il contorno a partire da un dato pixel
- approssimazione poligonale/spline (campionamento dei pixel del bordo prendendo un punto ogni n e si costruisce una curva poligonale a partire da essi)

## Etichettatura delle componenti connesse

Obiettivo: individuare automaticamente le diverse componenti connesse in un'immagine, assegnando loro delle etichette (tipicamente numeriche).

### Algoritmo

1. Scansione dell'immagine. $`\forall p \in F`$ si considerano i pixel di F vicini già visitati:
   - se nessuno è etichettato, si assegna una nuova etichetta a p
   - se uno è etichettato, si assegna a p la stessa etichetta
   - se più di uno è etichettato, si assegna a p una delle etichette e si annotano le equivalenze
2. Definizione di un'unica etichetta per ogni insieme di etichette marcate come equivalenti.
3. Seconda scansione dell'immagine: si assegnano le etichette finali

### Struttura dati per le etichette

Si fa uso di insiemi disgiunti (Disjoint Sets), dotati di operazioni particolarmente comode per questo tipo di elaborazione, tra cui `MakeSet(x)`, `MakeUnion(x, y)`, `FindRepresentative(x)`, `Renumber`.

## Thinning

**Scheletro** di un'immagine binaria:
- può essere definito come il luogo dei centri dei cerchi, tangenti in almeno due punti, che sono completamente contenuti nel foreground
- preserva le caratteristiche topologiche dell'oggetto
- viene ottenuto mediante algoritmi di thinning
  - gli algoritmi che cercano di implementare direttamente la definizione sono computazionalmente inefficienti

**Thinning** (assottigliamento):
- tipicamente iterativi
- basati sull'eliminazione dei pixel di bordo (attribuendoli a $`F^{\ast}`$) finché non vi sono più pixel da eliminare
- non alterano la topologia locale:
  - non rimuovono pixel terminali
  - non rimuovono pixel di connessione
  - non causano erosione eccessiva

### Algoritmo di Hilditch

Uno dei più noti algoritmi di thinning.

Siano:
- **A(p)** il numero di transizioni Background -> Foreground nella sequenza antioraria ordinata n0, n1, n2, n3, n4, n5, n6, n7, n0
- **B(p)** il numero di pixel appartenenti a F nell'8-intorno di p

| n3 | n2 | n1 |
|---|---|---|
| n4 | p | n0 |
| n5 | n6 | n7|

Ad ogni passo:
- l'algoritmo considera tutti i pixel in modo parallelo (ed è possibile farlo poiché l'operazione su un pixel non dipende da quelli già esaminati)
- i pixel p che rispettano le seguenti condizioni sono cancellati
  - 2 $`\leq`$ B(p) $`\leq`$ 6
  - A(p) = 1
  - n2 = back || n0 = back || n4 = back || A(n2) != 1
  - n2 = back || n0 = back || n6 = back || A(n0) != 1

L'algoritmo termina quando nessun nuovo pixel può essere cancellato.

Problemi che si possono incontrare:
- il rumore creerà una linea di thinning che viene mantenuta durante il processo
- un quadrato 2x2 viene eliminato
- una linea diagonale non viene mantenuta correttamente
