# Operazioni sulle immagini

[[_TOC_]]

## Immagini digitali

### Raster

Matrice di valori detti pixel (picture element) in cui ognuno rappresenta il dato campionato e quantizzato misurato da un sensore.

Caratteristiche:
- dimensione (espressa in larghezza per altezza: WxH Weight x Height)
- risoluzione (espressa in DPI)
- formato dei pixel (b/n, grayscale, colore)
- formato di memorizzazione (jpg, png, bmp...)
- formato di compressione
- occupazione in memoria senza compressione (W x H x depth dove depth = bit per pixel)

### Vettoriali

Insieme di primitive geometriche (linee, archi...), convertite in immagini raster e visualizzate.

## Colori

### Scala di grigi

- 8 bit per pixel (range valori [0, 255])
- memorizzate in array bidimensionale o monodimensionale (più efficiente)

### Immagini a colori

#### Palette

- 16 o 256 colori
- i valori dei pixel sono indici di una tavolozza (palette) di colori

#### RGB

- tipicamente 24 bpp
- ogni pixel ontiene un valore per ciascuna delle 3 componenti (8 bpp per Red, 8 bpp per Green, 8 bpp per Blue)

## Modelli per la rappresentazione dei colori

### RGB

#### RGB come modello additivo

- i colori sono ottenuti dalla combinazione dei 3 colori primari
- è il più usato in IT per la semplicità con cui si generano i colori

#### RGB come spazio tridimensionale

- i colori sono visti come punti in uno spazio a tre dimensioni
- non idoneo per il raggruppamento spaziale di colori percepiti come simili dall'uomo

### HS*

Modelli basati sulle proprietà tipicamente usate dall'uomo per descrivere un colore (quindi è più intuitivo):
- Tinta (Hue)
- Saturazione
- Luminosità (Value o Lightness)

#### HSV

- Rappresentazione mediante cono in cui l'asse verticale codifica V

#### HSL

- Rappresentazione mediante doppio cono in cui l'asse verticale codifica L
- Rappresenta meglio i concetti di Saturazione e Luminosità
  - muovendosi in S si passa da un tono di grigio (S = 0) al colore completamente saturo (S = 1)
  - muovendosi in L si passa dal nero (L = 0) al bianco (L = 1)
- Intervallo di valori
  - Hue [0 .. 2$`\pi`$]
  - Saturation [0 .. 1]
  - Value [0 .. 1]
- Approssimazioni:
  - lato codice si usano 3 byte, discretizzando i valori nell'intervallo [0 .. 255]
  - dal punto di vista geometrico anziché usare due coni si possono usare due piramidi (semplifica il calcolo)

### Conversioni RGB -> HSL

![image.png](./rgb_hsl.png)

### Conversioni HSL -> RGB

![image_1.png](./hsl_rgb.png)

## Lookup table

- Array che contiene il risultato della funzione f di mapping per ogni input
- particolarmente efficiente da usare se il numero di colori o di livelli di grigio è minore del numero di pixel dell'immagine
- la LUT può essere quindi applicata ad un'immagine per eseguire l'operazione su tutti i pixel

```csharp
Result[i] = lookupTable[InputImage[i]];
```

## Operazioni sui pixel

### Su singola immagine

Ogni pixel dell'immagine in uscita è funzione solo del corrispondente pixel dell'immagine di input

```math
I'[i, j] = f(I[i, j])
```

Esempi:
- variazione luminosità
  ```math
  pixel + \frac{variazione \cdot 255}{100}
  ```
- variazione livelli di grigio
- conversione grayscale -> pseudocolori
  ```csharp
  res[i] = lut[img[i]];
  ```
- binarizzazione con soglia globale
  ```csharp
  (byte)(pixel < threshold ? 0 : 255)
  ```

### Su più immagini

Ogni pixel dell'immagine in uscita è funzione solo **dei corrispondenti** pixel **delle immagini** di input

```math
I'[i, j] = f(I_{1}[i, j], I_{2}[i, j], ...)
```

Esempi: operazioni aritmetiche fra due immagini: somma, sottrazione, AND, OR, XOR ...

#### Differenza

```math
|Img1 - Img2|
```

```csharp
Result[i] = (byte) Math.Abs(img1[i] - img2[i])
```

#### AND

```csharp
Result[i] = (byte) (img1[i] & img2[i])
```

## Istogramma (di un'immagine grayscale)

Array che memorizza il numero di pixel per ogni livello di grigio.

```csharp
foreach (byte b in InputImage)
    Result[b]++;
```

### Analisi

- maggior parte dei valori in una singola zona -> poco contrasto
- valori predominanti in zone a bassa intensità -> immagine scura
- valori predominanti in zone ad alta intensità -> immagine chiara
- rende semplice la classificazione di oggetti se questi ultimi hanno differenti livelli di grigio all'interno dell'immagine (es. oggetto che si distingue dallo sfondo)
- fornisce informazioni utili per le operazioni sui pixel

## Contrast stretching

Aumenta il contrasto espandendo i livelli di grigio mediante un semplice mapping lineare

```math
f(g) = 255 \cdot \frac{g - min(g_{i})}{max(g_{i}) - min(g_{i})}
```

Il valore in output va da 0 a 1.

Per evitare che gli outliner compromettano la stima del minimo / massimo, si ignora una piccola percentuale dei pixel più chiari e più scuri prima della stima (es l'1%).

## Equalizzazione dell'istogramma

Si distribuisce l'istogramma su tutti i livelli di grigio (ovvero si distribuiscono equamente i pixel alle diverse intensità).

Rende confrontabili immagini catturate in diverse condizioni di illuminazione

```math
f(g) = \frac{255}{\#pixel} \cdot \int_{0}^{g}{H(w) dw} \approx f(g_{i}) \frac{255}{\#pixel} \cdot \sum_{j=0}^{i}{H[g_{i}]}
```

Per un'immagine che sfrutta il modello di colore RGB, l'equalizzazione viene fatta su tutti e tre i canali. Per un'immagine che basa il colore sul modello HSL, l'equalizzazione viene fatta soltanto sul canale L.

## Filtri digitali e convoluzione

**Filtri digitali**: Maschera discreta di pesi che indicano come ogni elemento dell'immagine debba essere modificato sulla base dei pixel vicini.

Sia F un filtro definito su una griglia mxm (con m dispari)

```math
I'[i, j] = \sum_{y = 1}^{m}{\sum_{x = 1}^{m}{(I[i + \lceil \frac{m}{2} \rceil - y, j + \lceil \frac{m}{2} \rceil - x] \cdot F[y, x])}}
```

Tale operazione di media pesata locale è detta **convoluzione** nel punto [i, j].

Esempio:

Sia m = 3

F:

assi: $`\rightarrow x`$ $`\downarrow y`$

| 4 | -2 | 1 |
|---|----|---|
|-1 | 5  | 3 |
|-6 | 0  | 4 |

I: 

| 30 | 28 | 32 |
|---|----|---|
| 27 | 26 | 10 |
| 29 | 22  | 18 |

allora

```math
I'[2, 2] = I[2 + 2 - 1, 2 + 2 - 1] \cdot F[1, 1] + ...
\\ = I[3, 3] \cdot F[1, 1] + ...
\\ = 18 \cdot 4 + ...
```

In pratica la convoluzione scorre il filtro F in modo lineare, con gli assi ribaltati e scorre I a partire dal fondo ("al contrario").

Se il risultato della convoluzione deve essere tra 0 e 255 lo si normalizza dividendolo per la somma dei pesi del filtro.

### Filtri di smoothing (sfocatura / regolarizzazione)

Utili come passo iniziale da effettuare prima di applicare altri operatori.

La sfocatura nasconde imperfezioni e brusche variazioni di luminosità.

### Filtri di sharpening (affinamento)

Evidenzia dettagli e brusche variazioni di luminosità (contorni).

Formula del filtro: $`I' = I + k \cdot R`$ dove I è l'immagine originale, k è un fattore che controlla la forza dell'effetto mentre R è l'immagine risultante dall'applicazione del filtro F.

Possono avere effetti indesiderati in presenza di rumore.

### Aspetti implementativi

- necessità di un buffer d'appoggio
- il risultato è una nuova immagine
- problema: i pixel ai bordi non hanno un intorno disponibile
  - si ignorano $`\lceil \frac{m}{2} \rceil`$ pixel di bordo ad ogni lato (ad esempio ponendo il loro valore a 0 nell'immagine risultante)
  - halo con intensità costante
- linearizzazione del filtro per una migliore efficienza
  - calcolando in anticipo gli offset dei soli elementi diversi da 0 rispetto alla posizione del pixel su cui applicare il filtro
  - in tale modo l'immagine e il filtro possono essere sfruttati come vettori monodimensionali
  - vantaggioso se molti elementi del filtro sono 0
- l'efficienza può anche essere migliorata separando i filtri
  - un filtro è separabile se può essere espresso come prodotto di un vettore colonna per un vettore riga
  - a livello pratico si applicano due filtri monodimensionali anziché uno bidimensionale
  - la complessità computazionale passa così da $`O(m^2n^2)`$ a $`O(2mn^2)`$

## Ruotare e ridimensionare un'immagine

Si applica prima un mapping diretto per applicare la trasformazione, poi un mapping inverso per risolvere eventuali problemi.

### Mapping diretto

Sia $`f : \Re x \Re \rightarrow \Re x \Re`$ una funzione che mappa ogni pixel dalla vecchia immagine alla nuova.

In caso di trasformazioni affini (traslazione $`[t_{x}, t_{y}]`$ + rotazione $`\theta`$ + scala $`s`$) la funzione è:

```math
\left[\begin{matrix} x_{new} \\ y_{new} \end{matrix} \right] =
\left[\begin{matrix} cos\theta & sin\theta \\ -sin\theta & cos\theta \end{matrix} \right] \cdot
\left[\begin{matrix} s & 0 \\ 0 & s \end{matrix} \right] \cdot
\left[\begin{matrix} x_{old} \\ y_{old} \end{matrix} \right] +
\left[\begin{matrix} t_{x} \\ t_{y} \end{matrix} \right]
```

I problemi prima citati in precedenza si riferiscono a:
- nuovi pixel che non necessariamente assumono valori interi
- alcuni pixel vengono mappati fuori dalla nuova immagine
- alcuni pixel nella nuova immagine hanno buchi

### Mapping inverso

Si cerca, per ogni pixel della nuova immagine, un punto di riferimento nella vecchia immagine mediante $`f^{-1}`$ così definita:

```math
\left[\begin{matrix} x_{old} \\ y_{old} \end{matrix} \right] =
\left[\begin{matrix} \frac{1}{s} & 0 \\ 0 & \frac{1}{s} \end{matrix} \right] \cdot
\left[\begin{matrix} cos(-\theta) & sin(-\theta) \\ -sin(-\theta) & cos(-\theta) \end{matrix} \right] \cdot
\left(
\left[\begin{matrix} x_{new} \\ y_{new} \end{matrix} \right] -
\left[\begin{matrix} t_{x} \\ t_{y} \end{matrix} \right]
\right)
```

Tale punto di riferimento, che è in coordinate continue (floating point), potrebbe cadere:
- fuori dalla vecchia immagine (e quindi si usa il colore di sfondo o nero)
- su un pixel della vecchia immagine (e si copia l'intensità)
- in una posizione intermedia tra 4 pixel della vecchia immagine (e si usa l'interpolazione di Lagrange: vedi slide)
