# Morfologia matematica

[[_TOC_]]

Branca della matematica derivante dalla teoria degli insiemi.

Obiettivo: estrarre informazioni utili a rappresentare e descrivere la forma delle immagini (contorno, scheletro...) rimuovendo particolari irrilevanti.

Lavora su immagini **binarie** ma esistono estensioni per immagini grayscale.

Definizione di **elemento strutturante**:
- formalmente "S"
- usato come parametro nelle operazioni morfologiche
- immagine binaria (2 livelli di grigio, 0 e 255)
- tipicamente quadrata di lato dispari (es. 3x3 o 5x5)
- centrata rispetto all'origine
- considerata come foreground

Le immagini binarie sono definite nel capitolo sulla [Topologia Digitale](03_TOPOLOGIA_DIGITALE.md).

Operazioni di base:
- intersezione: $`A \cap B = \{p|p \in A \land p \in B \}`$
- unione: $`A \cup B = \{p|p \in A \lor p \in B \}`$
- complemento: $`A^c = \{p|p \notin A \}`$
- differenza: $`A - B = A \cap B^c = \{p|p \in A \land p \notin B \}`$
- riflessione (rispetto a origine): $`A^r = \{p|p = - q, q \in A \}`$

## Operatori di base

Sono fondamentali per le operazioni morfologiche più complesse.

Per esempi ed implementazione vedi slide.

### Dilatazione

La nuova immagine è l'insieme dei pixel tali che, traslando in essi $`S^r`$ (ovvero l'elemento strutturante riflesso rispetto all'origine), almeno uno dei suoi elementi è sovrapposto a F.

Effetto: può aiutare a chiudere "buchi".

```math
F \oplus S = \{ q | (S^r)_{q} \cap F \neq \varnothing \}
```

### Erosione

La nuova immagine è l'insieme dei pixel tali che, traslando in essi S (ovvero l'elemento strutturante), S è interamente contenuto in F.

Effetto: può aiutare a eliminare il rumore.

```math
F \ominus S = \{ q | (S)_{q} \subseteq F \}
```

## Operatori composti

Per esempi ed implementazione vedi slide.

### Apertura (opening)

Erosione seguita da Dilatazione.

Utile per:
- separare oggetti debolmente connessi
- rimuove regioni piccole (es. togliere rumore)

```math
F \circ S = (F \ominus S) \oplus S
```

### Chiusura (closing)

Dilatazione seguita da Erosione.

Utile per:
- riempire buchi e piccole concavità
- rafforzare la connessione di regioni unite debolmente

```math
F \bullet S = (F \oplus S) \ominus S
```

### Hit-or-Miss transform

Localizza  punti in cui S1 è contenuto nel foreground e S2 nel background.

È un'operazione di template matching.

Sia $`S = (S_{1}, S_{2}), S_{1} \cap S_{2} = \varnothing`$

```math
F \ast S = (F \ominus S_{1}) \cap (F^c \ominus S_{2})
```

### Estrazione del contorno

L'elemento strutturante determina lo spessore del contorno.

Si sottrae all'immagine l'immagine erosa.

```math
\beta_{S} (F) = F - (F \ominus S)
```

## Morfologia in scala di grigio

Estensione degli operatori di base alle immagini grayscale.

Definizioni:
- f(x, y): immagine grayscale
- b(x, y): elemento strutturante (flat o non-flat) con origine posta al centro

### Operatori di base per gli elementi strutturanti flat

#### Dilatazione

Per ogni (x, y) si prende il valore **massimo** dell'immagine indicata da b quando l'origine di b si trova in (x, y).

Effetto ottenuto: le dimensioni delle componenti chiare aumentano, mentre diminuiscono per le componenti scure

```math
[f \oplus b](x, y) = max_{(s,t) \in b}\{ f(x - s, y - t) \}
```

#### Erosione

Per ogni (x, y) si prende il valore **minimo** dell'immagine indicata da b quando l'origine di b si trova in (x, y).

Effetto ottenuto: opposto della dilatazione.

```math
[f \ominus b](x, y) = min_{(s,t) \in b}\{ f(x + s, y + t) \}
```

### Operatori di base per gli elementi strutturanti non-flat

Il risultato degli operatori non è necessariamente limitato dai valori di f e ciò può portare a problemi nell'interpretazione dei risultati.

#### Dilatazione

```math
[f \oplus b_{NF}](x, y) = max_{(s,t) \in b_{NF}}\{ f(x - s, y - t) + b_{NF} (s, t) \}
```

#### Erosione

```math
[f \ominus b_{NF}](x, y) = min_{(s,t) \in b_{NF}}\{ f(x + s, y + t) - b_{NF} (s, t) \}
```
