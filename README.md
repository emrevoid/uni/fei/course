# Fondamenti di Elaborazione di Immagini

## Indice

1. [Operazioni sulle immagini](01_OPERAZIONI_IMMAGINI.md)
2. [Estrazione dei Bordi e Segmentazione](02_ESTR_BORDI_SEGMENTAZIONE.md)
3. [Topologia digitale](03_TOPOLOGIA_DIGITALE.md)
4. [Morfologia matematica](04_MORFOLOGIA_MATEMATICA.md)
5. [Template Matching](05_TEMPLATE_MATCHING.md)

---

- [Laboratori](https://gitlab.com/emrevoid/uni/fei/lab) (C# con Visual Studio)
- [Esami](https://gitlab.com/emrevoid/uni/fei/exam)
